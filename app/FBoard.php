<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FBoard extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'img', 'description'];


    /**
     * HasMany to FBoard
     *
     * @return Illuminate/Database/Eloquent/Relations/HasMany
     */
    public function threads()
    {
    	return $this->hasMany(\App\FThread::class);
    }
}
