<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\FThread;

class FMessage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['f_thread_id', 'user_id', 'content'];
    

    /**
     * BelongsTo to User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * BelongsTo to FThread
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thread()
    {
        return $this->belongsTo(FThread::class);
    }
}
