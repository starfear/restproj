<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FBoard;
use App\User;

class FThread extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['f_board_id', 'user_id', 'title', 'content'];
    
    /**
     * BelongsTo to User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * HasMany to FMessage
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->HasMany(FMessage::class);
    }
}
