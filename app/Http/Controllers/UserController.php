<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Rules\PasswordValid;
use App\User;
use Auth;

class UserController extends Controller
{
    /**
     * Me
     *
     * @param Request $req
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $req) : JsonResponse
    {
        return response()->json(Auth::user());
    }

    /**
	 * Change name
	 *
	 * @param Request $req
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function changename(Request $req) : JsonResponse
    {
    	$validator = Validator::make($req->all(), [
    		'name' => ['required', 'min:5', 'max:255', 'unique:users']
    	]);

    	if ($validator->fails())
    		return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()], 400);

    	$user = Auth::user();
    	$user->name = $req->name;
    	$user->save();
    	return response()->json([], 204); // [No content] All is ok, nothing to return
    }

    /**
	 * Change password
	 *
	 * @param Request $req
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function changepass(Request $req) : JsonResponse
    {
    	$validator = Validator::make($req->all(), [
    		'oldpassword' => ['required', 'max:255', new PasswordValid],
    		'password' => ['required', 'min:5', 'max:255', 'confirmed']
    	]);

    	if ($validator->fails())
    		return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()], 400);

    	$user = Auth::user();
    	$user->password = Hash::make($req->password);
    	$user->save();

    	return response()->json([], 204); // [No content] OK, nothing to return
    }
}
