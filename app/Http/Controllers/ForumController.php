<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\FThread;
use App\FBoard;
use App\FMessage;
use App\User;

use App\Http\Resources\FBoard as FBoardResource;
use App\Http\Resources\FThread as FThreadResource;
use App\Http\Resources\FMessage as FMessageResource;

Use Auth;

class ForumController extends Controller
{
    /**
     * listOfBoards
     * r
     * @param Request $req
     * @return \Illuminate\Http\JsonResponse
     */
    public function listOfBoards(Request $req)
    {
    	return FBoardResource::collection(FBoard::get());
    }

    /**
     * listOfThreads
     * 
     * @param Request $req
     * @return \Illuminate\Http\JsonResponse
     */
    public function listOfThreads(Request $req)
    {
    	$validator = Validator::make($req->all(), ['offset' => 'integer', 'board' => 'required|integer|exists:f_boards,id']);
    	
    	if ($validator->fails())
    		return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()], 400);

    	$threads = FThread::where('f_board_id', $req->board)->limit(20);

    	if($req->offset)
    		$threads->offset($req->offset);

    	return FThreadResource::collection($threads->get());
    }

    /**
     * listOfMessages
     * 
     * @param Request $req
     * @return \Illuminate\Http\JsonResponse
     */
    public function listOfMessages(Request $req)
    {
    	$validator = Validator::make($req->all(), ['thread' => 'required|integer|exists:f_boards,id']);
    	
    	if ($validator->fails())
    		return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()], 400);

    	return FMessageResource::collection(FMessage::get()->where('f_thread_id', $req->thread));
    }

    /**
     * storeThread
     * 
     * @param Request $req
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeThread(Request $req)
    {
    	$validator = Validator::make($req->all(), [
    		'title' => 'required|string|min:3|max:1024',
    		'content' => 'required|string|min:3|max:65536',
    		'board' => 'required|integer|exists:f_boards,id'
    	]);

    	if ($validator->fails())
    		return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()], 400);

    	$thread = FThread::create([
    		'title' => $req->title,
    		'content' => $req->content,
    		'f_board_id' => $req->board,
    		'user_id' => Auth::user()->id
    	]);

    	return response()->json(new FThreadResource($thread), 203);
    }

    /**
     * storeMessage
     * 
     * @param Request $req
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeMessage(Request $req)
    {
    	$validator = Validator::make($req->all(), [
    		'content' => 'required|string|min:3|max:1024',
    		'thread' => 'required|integer|exists:f_threads,id'
    	]);

    	if ($validator->fails())
    		return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()], 400);

    	$message = FMessage::create([
    		'content' => $req->content,
    		'f_thread_id' => $req->thread,
    		'user_id' => Auth::user()->id
    	]);

    	return response()->json(new FMessageResource($message), 203);
    }
}
