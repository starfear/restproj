<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\JsonResponse;
use App\User;
use JWTAuth;

class AuthController extends Controller
{
	/**
	 * Login user
	 *
	 * @param Request $req
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function login(Request $req) : JsonResponse
    {
    	if (! $token = JWTAuth::attempt($req->only('email', 'password')))
    		return response()->json(['status' => 'invalid_credentials', 400]);

    	return response()->json(compact('token'), 200);
    }

	/**
	 * Register user
	 *
	 * @param Request $req
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function register(Request $req) : JsonResponse
    {
    	$validator = Validator::make($req->all(), [
    		'name' => ['required', 'string', 'max:255', 'unique:users'],
    		'email' => ['required', 'email', 'max:255', 'unique:users'],
    		'password' => ['required', 'max:255', 'confirmed'],
    	]);

    	if ($validator->fails())
    		return response()->json(['status' => 'validation_error', 'errors' => $validator->errors()], 400);

    	$user = User::create([
    		'name' => $req->name,
    		'email' => $req->email,
    		'password' => Hash::make($req->password)
    	]);

    	$token = JWTAuth::fromUser($user);
    	

    	return response()->json(compact('user', 'token'), 201);
    }
}
