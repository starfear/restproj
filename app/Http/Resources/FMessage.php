<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FMessage extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'thread' => $this->f_thread_id,
            'content' => $this->content,
            'created_at' => $this->created_at
        ];
    }
}
