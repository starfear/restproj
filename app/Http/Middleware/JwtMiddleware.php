<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * errors: [token_invalid, token_expired, unauthorized]
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            // check if user exists
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            // if there is no user with this token
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException)
                return response()->json(['status' => 'token_invalid'], 400); // Bad request
            else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException)
                return response()->json(['status' => 'token_expired'], 406); // Unacceptable
            else
                return response()->json(['status' => 'unauthorized'], 401); // Unauthorized
        }

        return $next($request);
    }
}
