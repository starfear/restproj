<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function() {

	/**
	 * Register
	 * Register user by given email, name, password
	 * @param email		name	password	password_confirmation
	 * @return User user	JWTToken token
	 */
	Route::put('register', 'AuthController@register');
	
	/**
	 * Login
	 * Authenticate user and return his JWT token
	 * @param email		password
	 * @return JWTToken token
	 */
	Route::get('login', 'AuthController@login');

	/**
	 * Forum Application Actions
	 * 
	 * @param header:Authorization
	 */
	Route::group(['prefix' => 'forum', 'middleware' => 'jwt.verify'], function () {
		Route::group(['prefix' => 'board'], function (){
			/**
			 * Get boards
			 * 
			 * @return CollectionOfBoards
			 */
			Route::get('list', 'ForumController@listOfBoards');
		});

		Route::group(['prefix' => 'thread'], function (){
			/**
			 * Get threads by given board
			 * 
			 * @param board		offset(o)
			 * @return CollectionOfThreads or validation_error
			 */
			Route::get('list', 'ForumController@listOfThreads');
			/**
			 * Create thread
			 * 
			 * @param board		title	content
			 * @return threadid or validation_error
			 */
			Route::get('create', 'ForumController@storeThread');
		});

		Route::group(['prefix' => 'message'], function (){
			/**
			 * Get messages by given thread
			 * 
			 * @param thread
			 * @return CollectionOfMessages or validation_error
			 */
			Route::get('list', 'ForumController@listOfMessages');

			/**
			 * Get messages by given thread
			 * 
			 * @param thread	content
			 * @return CollectionOfMessages or validation_error
			 */
			Route::get('create', 'ForumController@storeMessage');
		});
	});
	
	/**
	 * User actions
	 * 
	 * @param header:Authorization
	 */
	Route::group(['prefix' => 'user', 'middleware' => 'jwt.verify'], function() {
		/**
		 * Me
		 * Return user info by given jwt token
		 * @return User user
		 */
		Route::get('me', 'UserController@me');

		/**
		 * changename
		 * Change user name to given name
	 	 * @param name
		 * @return Nothing or validation_error
		 */
		Route::post('changename', 'UserController@changename');

		/**
		 * changepass
		 * Change user password to given password
		 * @param oldpassword		password		password_confirmation
		 * @return Nothing or validation_error
		 */
		Route::post('changepass', 'UserController@changepass');
	});
});

Route::fallback(function() {
	return response()->json(['status' => 'not_found'], 404);
});
