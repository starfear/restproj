<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('f_messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('f_thread_id')->default(0);
            $table->integer('user_id')->default(0);
            $table->text('content', 65536);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('f_messages');
    }
}
