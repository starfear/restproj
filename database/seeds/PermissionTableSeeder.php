<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $perms = [
        	'catalog' => [
        		'get',
        		'post',
        		'put',
        		'delete'
        	],

        	'product' => [
        		'get',
        		'post',
        		'put',
        		'delete'
        	],
        ];

        // $⠀⠀⠀⠀ = function ($⠀, String $⠀⠀⠀⠀⠀⠀ = "") use (&$⠀⠀⠀⠀) {foreach ($⠀ as $⠀⠀ => $⠀⠀⠀) if (is_array($⠀⠀⠀)) $⠀⠀⠀⠀($⠀⠀⠀, "$⠀⠀⠀⠀⠀⠀$⠀⠀."); else Permission::create(['name' => "$⠀⠀⠀⠀⠀⠀$⠀⠀⠀"]); }; for minify xd


        $f = function ($a, String $p = "") use (&$f) {
        	foreach ($a as $k => $v)
	        	if (is_array($v))
	        		$f($v, "$p$k.");
	        	else
	        		Permission::create(['name' => "$p$v"]);
        };

        $f($perms);
    }
}
