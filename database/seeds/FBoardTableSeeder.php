<?php

use Illuminate\Database\Seeder;
use App\FBoard;
use App\FThread;
use App\FMessage;
use App\User;

class FBoardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	FBoard::create([
    		'name' => 'abc',
    		'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque consequuntur exercitationem provident itaque nesciunt fugiat fuga numquam distinctio culpa quia totam in illo quisquam reiciendis aperiam alias ullam, iure? Voluptatum.',
    		'img' => 'test'
    	]);

    	foreach (range(0, 100) as $i) {
	    	FThread::create([
	    		'user_id' => User::first()->id,
	    		'f_board_id' => FBoard::first()->id,
	    		'title' => "thread $i",
	    		'content' => 'hey guys'
	    	]);
    	}

    	foreach (range(0, 100) as $i) {
    		FMessage::create([
    			'user_id' => User::first()->id,
    			'f_thread_id' => FThread::first()->id,
    			'content' => "Message number $i"
    		]);
    	}
    }
}
